import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {ActivatedRoute} from '@angular/router';
import {get} from 'lodash-es';
import {finalize} from 'rxjs/operators';

@Component({
	selector: 'app-redirect-landing',
	templateUrl: './redirect-landing.component.html',
	styleUrls: ['./redirect-landing.component.scss']
})
export class RedirectLandingComponent implements OnInit {

	public response: any;
	public authRoute: string;
	public error: string | null = null;
	public loading: boolean = true;

	constructor(private authService: AuthService, private route: ActivatedRoute) {
	}

	ngOnInit(): void {
		this.authRoute = localStorage.getItem('xano_linkedin_demo_route');
		this.route.queryParams.subscribe(params => {
			const code = params['code'];
			if (code) {
				switch (this.authRoute) {
					case 'login':
						this.authService.linkedinLogin(code)
							.pipe(finalize(() => this.loading = false))
							.subscribe(res => {
								this.response = res;
							}, error => this.error = get(error, 'error.message', 'An error occurred'));
						break;
					case 'signup':
						this.authService.linkedinSignUp(code)
							.pipe(finalize(() => this.loading = false))
							.subscribe(res => {
								this.response = res;
							}, error => this.error = get(error, 'error.message', 'An error occurred'));
						break;
					case 'continue':
						this.authService.linkedinContinue(code)
							.pipe(finalize(() => this.loading = false))
							.subscribe(res => {
								this.response = res;
							}, error => this.error = get(error, 'error.message', 'An error occurred'));
						break;
					default:
						break;
				}
				localStorage.removeItem('xano_linkedin_demo_route');
				localStorage.removeItem('xano_api_url');
			}
		});

	}

}
