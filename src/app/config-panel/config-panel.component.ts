import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {FormControl, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
	selector: 'app-config-panel',
	templateUrl: './config-panel.component.html',
	styleUrls: ['./config-panel.component.scss']
})
export class ConfigPanelComponent implements OnInit {
	public config: XanoConfig;

	public configFormControl: FormControl = new FormControl('',
		[Validators.required, Validators.pattern('^https?://.+')]
	);

	constructor(
		private configService: ConfigService,
		private dialogRef: MatDialogRef<ConfigPanelComponent>,
		private snackBar: MatSnackBar
	) {
		this.config = this.configService.config;
	}

	ngOnInit(): void {
		this.configFormControl.patchValue(this.configService.xanoApiUrl.value);

	}

	public submit(): void {
		this.configFormControl.markAsTouched();
		if (this.configFormControl.valid) {
			this.configService.configGet(this.configFormControl.value).subscribe(res => {
				if (!this.configService.config.requiredApiPaths.every(path => res.includes(path))) {
					const message = 'This Xano Base URL is missing the required endpoints. Have you installed this marketplace extension?';
					this.snackBar.open(message, 'Error');
				} else {
					this.configService.xanoApiUrl.next(this.configFormControl.value);
					this.dialogRef.close();
				}

			}, error => {
				this.snackBar.open('An error has occurred', 'Error', {panelClass: 'error-snack'});
			});

		}
	}

}
