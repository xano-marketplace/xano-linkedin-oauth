import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ConfigService} from './config.service';
import {ApiService} from './api.service';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private configService: ConfigService, private apiService: ApiService) {
	}

	public linkedinInit(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/linkedin/init`,
			params: {
				redirect_uri: this.configService.redirectUri
			}
		});
	}

	public linkedinLogin(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/linkedin/login`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		});
	}

	public linkedinSignUp(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/linkedin/signup`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		});
	}

	public linkedinContinue(code): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/oauth/linkedin/continue`,
			params: {
				redirect_uri: this.configService.redirectUri,
				code: code
			}
		});
	}
}
